create table if not exists produit
(
    id           int auto_increment
        primary key,
    nom          varchar(40) not null,
    description  longtext    null,
    prix         double      null,
    image        varchar(60) null,
    categorie_id int         not null,
    constraint FK_29A5EC27BCF5E72D
        foreign key (categorie_id) references categorie (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_29A5EC27BCF5E72D
    on produit (categorie_id);

