create table if not exists categorie
(
    id  int auto_increment
        primary key,
    nom varchar(30) not null
)
    collate = utf8mb4_unicode_ci;

