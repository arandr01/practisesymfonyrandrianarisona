<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Produit;
use App\Form\ProduitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/default', name: 'homepage')]
    public function index(): Response
    {
        return $this->render('default/index.html.twig');
    }

    #[Route('/produit/test', name: 'produitTest')]
    public function create1produit() : Response
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $categorie = $categories[rand(0,count($categories)-1)];
        $produit = new Produit("produit ".rand(0,10),"ceci est une description",10,"logo.png",$categorie);
        $em = $this->getDoctrine()->getManager();
        $em->persist($categorie);
        $em->persist($produit);
        $em->flush();
        return $this->render('default/produitTest.html.twig',['produit' => $produit]);
    }
    #[Route('/produit', name: 'produit')]
    public function produit() : Response
    {
        $produits = $this->getDoctrine()->getRepository(Produit::class)->findAll();
        return $this->render('default/produit.html.twig',['produits' => $produits]);
    }

    #[Route('/categories', name: 'categories')]
    public function categorie() : Response
    {
        $categorie = new Categorie("categorie ".rand(0,10));
        $em = $this->getDoctrine()->getManager();
        $em->persist($categorie);
        $em->flush();
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        return $this->render('default/categorie.html.twig',['categories' => $categories]);
    }

    #[Route('/produit/ajout', name: 'produitAjout')]
    public function produitAjout(Request $request) : Response
    {
        $form = $this->createForm(ProduitType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            var_dump($data);
        }
        $formView = $form->createView();
        return $this->render('default/produitAjout.html.twig',['form' => $formView]);
    }

    #[Route('/categorie/', name: 'categorie')]
    public function categories(Request $request) : Response
    {
        return $this->render('default/categories.html.twig');
    }
}
