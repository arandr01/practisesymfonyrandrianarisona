<?php

namespace App\Repository;

use App\Entity\CollectionCategorie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CollectionCategorie|null find($id, $lockMode = null, $lockVersion = null)
 * @method CollectionCategorie|null findOneBy(array $criteria, array $orderBy = null)
 * @method CollectionCategorie[]    findAll()
 * @method CollectionCategorie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CollectionCategorieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CollectionCategorie::class);
    }

    // /**
    //  * @return CollectionCategorie[] Returns an array of CollectionCategorie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CollectionCategorie
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
