<?php

namespace App\DataFixtures;
use App\Entity\Categorie;
use App\Entity\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProduitFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $tabCategorie = array();
        for($i = 0; $i < 20; $i++){
            $categorie = new Categorie('categorie'.$i);
            $tabCategorie[] = $categorie;
            $manager->persist($categorie);
        }

        for($j = 0; $j < 20; $j++){
            $produit = new Produit('produit'.$j,null,rand(0,10),null,$tabCategorie[rand(0,count($tabCategorie)-1)]);
            $manager->persist($produit);
        }

        $manager->flush();
    }
}
